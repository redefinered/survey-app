<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\RsVisitor;

class ReputationSystemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('rps.step1');
    }

    public function test()
    {
        $visitors = RsVisitor::all();
        var_dump($visitors);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeStep1(Request $request) {

        $emails = RsVisitor::all();
        foreach ($emails as $email) {
            $x[] = $email->email;
        }
        if(isset($x)) {
            if(in_array($request->input('rpfs-email'), $x)) {
                return redirect('reputation-system')->with('emailExistsMessage', $request->input('rpfs-email') . ' was already taken. Please choose another email.');
            }
        }

        $messages = [
            'rpfs-firstname.required' => 'The firstname field is required!',
            'rpfs-lastname.required' => 'The lastname field is required!',
            'rpfs-email.required' => 'We need to know your e-mail address!',
        ];

        $this->validate($request, [
            'rpfs-firstname'        => 'required',
            'rpfs-lastname'         => 'required',
            'rpfs-email'            => 'email|required'
            ], $messages);

        $randStringTempVisitorID = 'tmp_visitor_' . $this->generateRandomString(8);

        $visitor = new RsVisitor;

        $visitor->visitor_id    = $randStringTempVisitorID;
        $visitor->status        = $request->input('rpfs-status');
        $visitor->firstname     = $request->input('rpfs-firstname');
        $visitor->lastname      = $request->input('rpfs-lastname');
        $visitor->email         = $request->input('rpfs-email');

        if($visitor->save()) {
            setcookie('rs_visitor_id', $randStringTempVisitorID, time() + (86400 * 30), '/');
            return redirect('step2');
        }

    }

    public function storeStep2(Request $request) {
        
        $messages = [
            'rpfs-recommendation.required' => 'Your feedback is important to us! Please choose one.',
            ];

        $this->validate($request, [
            'rpfs-recommendation'       => 'required',
            ], $messages);

        $tmp_visitor = RsVisitor::where('visitor_id', $_COOKIE['rs_visitor_id'])->first();
        if($tmp_visitor != null) {
            $tmp_visitor->recommendation = $request->input('rpfs-recommendation');
        }
        if($tmp_visitor->save()) {
            if($request->input('rpfs-recommendation') == 'yes') {
                return redirect('social-review-options')->with('visitor_id', $request->input('rpfs-visitor-id'));    
             } else {
                return view('rps.not-recommended')->with('visitor_id', $request->input('rpfs-visitor-id'));
             }            
        } else {
            return 'data save failed';
        }
    }

    public function socialReview() {
        return view('rps.social-review-options');
    }

    public function storeSocialOptions(Request $request) {
        $tmp_visitor = RsVisitor::where('visitor_id', $_COOKIE['rs_visitor_id'])->first();
        if($tmp_visitor != null) {
            $tmp_visitor->visitor_id        = 'user_' . $tmp_visitor->id;
            $tmp_visitor->status            = 'completed';    
            $tmp_visitor->social_option     = $request->input('rpfs-social-option');
        }
        if($tmp_visitor->save()) {
            setcookie('rs_visitor_id', '', time() - 3600);
            return redirect('reputation-system')->with('completed', 'Survey complete! Thanks for your time.');
        }
    }

    public function complete() {
        return view('rps.complete');
    }

    public function storeComment(Request $request) {

        $messages = [
            'rpfs-comment.required' => 'Please leave us a comment.',
            ];

        $this->validate($request, [
            'rpfs-comment'        => 'required',
            ], $messages);

        $tmp_visitor = RsVisitor::where('visitor_id', $_COOKIE['rs_visitor_id'])->first();
        $tmp_visitor->visitor_id        = 'user_' . $tmp_visitor->id;
        $tmp_visitor->status            = 'completed';
        $tmp_visitor->comment           = $request->input('rpfs-comment');
        if($tmp_visitor->save()) {
            setcookie('rs_visitor_id', '', time() - 3600);
            return redirect('reputation-system')->with('completed', 'Thanks for your time.');
        }
    }

    private function crypto_rand_secure($min, $max)
    {
        $range = $max - $min;
        if ($range < 1) return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd >= $range);
        return $min + $rnd;
    }

    private function generateRandomString($length = 64)
    {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet) - 1;
        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->crypto_rand_secure(0, $max)];
        }
        return $token;
    }
}
