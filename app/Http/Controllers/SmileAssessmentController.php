<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\SaVisitor;

class SmileAssessmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('sa.index');
    }

    public function step1() {
        return view('sa.step1');
    }

    public function storeStep1(Request $request) {

        $messages = [
            'sa-age.required' => 'We need to know you age!',
        ];

        $this->validate($request, [
            'sa-age' => 'required',
            ], $messages);

        $randStringTempVisitorID = 'tmp_visitor_' . $this->generateRandomString(8);

        $visitor = new SaVisitor;
        $visitor->visitor_id    = $randStringTempVisitorID;
        $visitor->status        = $request->input('sa-status');
        $visitor->age           = $request->input('sa-age');
        if($visitor->save()) {
            setcookie('sa_visitor_id', $randStringTempVisitorID, time() + (86400 * 30), '/');
            return redirect('sa-step2');
        }

    }

    public function storeStep2(Request $request) {

        $messages = [
            'sa-teeth-condition.required' => 'This field is required!',
        ];

        $this->validate($request, [
            'sa-teeth-condition' => 'required',
            ], $messages);

        $visitor = SaVisitor::where('visitor_id', $_COOKIE['sa_visitor_id'])->first();
        $visitor->teeth_condition = $request->input('sa-teeth-condition');
        if($visitor->save()) {
            return redirect('sa-step3');
        }

    }

    public function storeStep3(Request $request) {
          
        $messages = [
            'sa-gum-condition.required' => 'This field is required!',
        ];

        $this->validate($request, [
            'sa-gum-condition' => 'required',
            ], $messages);

        $visitor = SaVisitor::where('visitor_id', $_COOKIE['sa_visitor_id'])->first();
        $visitor->gum_condition = $request->input('sa-gum-condition');
        if($visitor->save()) {
            return redirect('sa-step4');
        }

    }

    public function storeStep4(Request $request) {
        
        $messages = [
            'sa-last-visit.required' => 'This field is required!',
        ];

        $this->validate($request, [
            'sa-last-visit'        => 'required',
            ], $messages);

        $visitor = SaVisitor::where('visitor_id', $_COOKIE['sa_visitor_id'])->first();
        $visitor->dentist_last_visit = $request->input('sa-last-visit');
        if($visitor->save()) {
            return redirect('sa-step5');
        }

    }

    public function storeStep5(Request $request) {
        
        $messages = [
            'sa-teeth-appearance.required' => 'This field is required!',
        ];

        $this->validate($request, [
            'sa-teeth-appearance' => 'required',
            ], $messages);

        $visitor = SaVisitor::where('visitor_id', $_COOKIE['sa_visitor_id'])->first();
        $visitor->teeth_appearance = $request->input('sa-teeth-appearance');
        if($visitor->save()) {
            return redirect('sa-step6');
        }

    }

    public function storeStep6(Request $request) {
        
        $messages = [
            'sa-fix-preferrence.required' => 'Please select at least one option!',
        ];

        $this->validate($request, [
            'sa-fix-preferrence' => 'required',
            ], $messages);

        $preferenceSave = json_encode($request->input('sa-fix-preferrence'));
        $visitor = SaVisitor::where('visitor_id', $_COOKIE['sa_visitor_id'])->first();
        $visitor->teeth_fix_preferrence = $preferenceSave;
        if($visitor->save()) {
            return redirect('sa-step7');
        }

    }

    public function storeStep7(Request $request) {
        
        $emails = SaVisitor::all();
        foreach ($emails as $email) {
            $x[] = $email->email;
        }
        if(isset($x)) {
            if(in_array($request->input('sa-email'), $x)) {
                return redirect('sa-step7')->with('emailExistsMessage', $request->input('sa-email') . ' was already taken. Please choose another email.');
            }
        }

        $messages = [
            'sa-firstname.required'     => 'The firstname field is required!',
            'sa-lastname.required'      => 'The lastname field is required!',
            'sa-email.required'         => 'We need to know your email address!',
        ];

        $this->validate($request, [
            'sa-firstname'      => 'required',
            'sa-lastname'       => 'required',
            'sa-email'          => 'email|required',
            ], $messages);

        $visitor = SaVisitor::where('visitor_id', $_COOKIE['sa_visitor_id'])->first();
        $visitor->visitor_id    = 'user_' . $visitor->id;
        $visitor->status        = 'completed';
        $visitor->firstname     = $request->input('sa-firstname');
        $visitor->lastname      = $request->input('sa-lastname');
        $visitor->email         = $request->input('sa-email');
        $visitor->phone         = $request->input('sa-phone');
        if($visitor->save()) {
            setcookie('sa_visitor_id', '', time() - 3600);
            return redirect('smile-assessment')->with('completed', 'Survey complete! Thanks for your time.');
        }

    }

    private function crypto_rand_secure($min, $max)
    {
        $range = $max - $min;
        if ($range < 1) return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd >= $range);
        return $min + $rnd;
    }

    private function generateRandomString($length = 64)
    {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet) - 1;
        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->crypto_rand_secure(0, $max)];
        }
        return $token;
    }
}
