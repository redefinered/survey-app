<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;

Route::get('/', function(){
	return view('welcome');
});

// Reputation System
Route::get('reputation-system', 'ReputationSystemController@index');
Route::post('reputation-system', 'ReputationSystemController@storeStep1');

Route::get('step2', function(){
	return view('rps.step2');
});
Route::post('step2', 'ReputationSystemController@storeStep2');

Route::get('social-review-options', 'ReputationSystemController@socialReview');
Route::post('social-review-options', 'ReputationSystemController@storeSocialOptions');

Route::get('not-recommended', function(){
	return view('rps.not-recommended');
});
Route::post('not-recommended', 'ReputationSystemController@storeComment');

// Smile Assessment
Route::get('smile-assessment', 'SmileAssessmentController@index');
Route::get('sa-step1', 'SmileAssessmentController@step1');

Route::get('sa-step2', function(){
	return view('sa.step2');
});

Route::get('sa-step3', function(){
	return view('sa.step3');
});

Route::get('sa-step4', function(){
	return view('sa.step4');
});

Route::get('sa-step5', function(){
	return view('sa.step5');
});

Route::get('sa-step6', function(){
	return view('sa.step6');
});

Route::get('sa-step7', function(){
	return view('sa.step7');
});

Route::post('sa-step1', 'SmileAssessmentController@storeStep1');
Route::post('sa-step2', 'SmileAssessmentController@storeStep2');
Route::post('sa-step3', 'SmileAssessmentController@storeStep3');
Route::post('sa-step4', 'SmileAssessmentController@storeStep4');
Route::post('sa-step5', 'SmileAssessmentController@storeStep5');
Route::post('sa-step6', 'SmileAssessmentController@storeStep6');
Route::post('sa-step7', 'SmileAssessmentController@storeStep7');