<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblSurveyVisitorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rs_visitors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('visitor_id', 64)->unique();
            $table->string('status', 20);
            $table->string('firstname', 100);
            $table->string('lastname', 100);
            $table->string('email', 100)->unique();
            $table->string('recommendation', 5)->nullable();
            $table->string('social_option');
            $table->string('comment', 300);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rs_visitors');
    }
}
