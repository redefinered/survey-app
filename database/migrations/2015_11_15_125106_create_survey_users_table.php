<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sa_visitors', function(Blueprint $table){
            $table->increments('id');
            $table->string('visitor_id', 64)->unique();
            $table->string('status', 20);
            $table->string('age', 50);
            $table->string('teeth_condition', 50);
            $table->string('gum_condition', 50);
            $table->string('dentist_last_visit', 50);
            $table->string('teeth_appearance', 50);
            $table->string('teeth_fix_preferrence', 200);
            $table->string('firstname', 100);
            $table->string('lastname', 100);
            $table->string('phone', 20);
            $table->string('email', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sa_visitors');
    }
}
