<div class="page-header text-center">
	<h1>Satisfaction Survey</h1>
	<p>Help us to improve our service and customer satisfaction.</p>
</div>

@if (count($errors) > 0)
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif