@include('partials.header')

<section class="container" id="main">
	<div id="survey_container">
		
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">

				@include('partials.form-head')

				<p class="text-center form-progress">Progress</p>
				<div class="progress">
					<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 75%;">
						75%
					</div>
				</div>

				<div class="step-title">
					<h3>What would you change about your teeth? (Select all that apply)</h3>
				</div>

				{!! Form::open(array('url' => '/sa-step6' )) !!}
					
					<div class="row">
						<div class="col-md-6">
							<div class="checkbox">
								<label>
									{!! Form::checkbox('sa-fix-preferrence[]', 'Make them brighter') !!}
									Make them brighter
								</label>
							</div>
							<div class="checkbox">
								<label>
									{!! Form::checkbox('sa-fix-preferrence[]', 'Make them straighter') !!}
									Make them straighter
								</label>
							</div>
							<div class="checkbox">
								<label>
									{!! Form::checkbox('sa-fix-preferrence[]', 'Reduce the gaps') !!}
									Reduce the gaps
								</label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="checkbox">
								<label>
									{!! Form::checkbox('sa-fix-preferrence[]', 'Replace old metal fillings with natural looking fillings') !!}
									Replace old metal fillings with natural looking fillings
								</label>
							</div>
							<div class="checkbox">
								<label>
									{!! Form::checkbox('sa-fix-preferrence[]', 'Replace missing teeth') !!}
									Replace missing teeth
								</label>
							</div>
							<div class="checkbox">
								<label>
									{!! Form::checkbox('sa-fix-preferrence[]', 'Improve my smile') !!}
									Improve my smile
								</label>
							</div>
						</div>
					</div>

					{!! Form::submit('next step', array('class' => 'btn btn-primary')) !!}

				{!! Form::close() !!}

				</div>
			</div>
		</div>
		
	</div>
</section>

@include('partials.footer')