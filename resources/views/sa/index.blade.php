@include('partials.header')

<section class="container" id="main">
	<div id="survey_container">
		
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">

				@if(session('completed'))
					<div class="text-center completed">
						<h1 class="text-center"><i class="fa fa-check-square-o"></i></h1>
						<h3>{{ session('completed') }}</h3>
						<?php $progress = 100; ?>
					</div>
				@else
					@include('partials.form-head')
					<?php $progress = 25; ?>
				@endif

					<p class="text-center form-progress">Progress</p>
					<div class="progress">
						<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="<?php echo $progress ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $progress ?>%;">
							<?php echo $progress . '%'; ?>
						</div>
					</div>

					<p>The most recognizable feature on any face is a smile. At Howard Family Dental, we believe that everyone should have the opportunity to proudly smile with confidence.</p>
					<p>If your smile makes you nervous or embarassed because of chipped, crooked or missing teeth or any of a dozen other cosmetic dental concerns; Call for a free Smile Assessment and let us discuss the miracles of cosmetic dentistry and what it can do for your smile.</p>
					<a href="sa-step1" class="btn btn-success">Start Assessment</a>

				</div>
			</div>
		</div>
		
	</div>
</section>

@include('partials.footer')