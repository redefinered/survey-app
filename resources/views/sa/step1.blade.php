@include('partials.header')

<section class="container" id="main">
	<div id="survey_container">
		
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">

				@include('partials.form-head')

				<p class="text-center form-progress">Progress</p>
				<div class="progress">
					<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 12.5%;">
						12.5%
					</div>
				</div>

				<div class="step-title">
					<h3>What is your age?</h3>	
				</div>

				{!! Form::open(array('url' => '/sa-step1' )) !!}

					{!! Form::hidden('sa-status', 'incomplete') !!}
					
					<div class="radio">
						<label>
							{!! Form::radio('sa-age', 'Under 18 yrs old') !!}
							Under 18 yrs old
						</label>
					</div>

					<div class="radio">
						<label>
							{!! Form::radio('sa-age', '18 - 40 yrs old') !!}
							18 - 40 yrs old
						</label>
					</div>

					<div class="radio">
						<label>
							{!! Form::radio('sa-age', '41 - 65 yrs old') !!}
							41 - 65 yrs old
						</label>
					</div>

					<div class="radio">
						<label>
							{!! Form::radio('sa-age', '65 and over') !!}
							65 and over
						</label>
					</div>

					{!! Form::submit('next step', array('class' => 'btn btn-primary')) !!}

				{!! Form::close() !!}

				</div>
			</div>
		</div>
		
	</div>
</section>

@include('partials.footer')