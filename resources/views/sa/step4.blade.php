@include('partials.header')

<section class="container" id="main">
	<div id="survey_container">
		
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">

				@include('partials.form-head')

				<p class="text-center form-progress">Progress</p>
				<div class="progress">
					<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 50%;">
						50%
					</div>
				</div>

				<div class="step-title">
					<h3>Your last visit to the dentist was</h3>
				</div>

				{!! Form::open(array('url' => '/sa-step4' )) !!}
					
					<div class="radio">
						<label>
							{!! Form::radio('sa-last-visit', 'Withing 1 Year') !!}
							Withing 1 Year
						</label>
					</div>

					<div class="radio">
						<label>
							{!! Form::radio('sa-last-visit', '1 to 3 years') !!}
							1 to 3 years
						</label>
					</div>

					<div class="radio">
						<label>
							{!! Form::radio('sa-last-visit', 'more than 3 years ago') !!}
							more than 3 years ago
						</label>
					</div>

					{!! Form::submit('next step', array('class' => 'btn btn-primary')) !!}

				{!! Form::close() !!}

				</div>
			</div>
		</div>
		
	</div>
</section>

@include('partials.footer')