@include('partials.header')

<section class="container" id="main">
	<div id="survey_container">
		
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">

				@include('partials.form-head')

				<p class="text-center form-progress">Progress</p>
				<div class="progress">
					<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 25%;">
						25%
					</div>
				</div>

				<div class="step-title">
					<h3>Are your teeth loose, worn or missing?</h3>
				</div>

				{!! Form::open(array('url' => '/sa-step2' )) !!}
					
					<div class="radio">
						<label>
							{!! Form::radio('sa-teeth-condition', 'yes') !!}
							Yes
						</label>
					</div>

					<div class="radio">
						<label>
							{!! Form::radio('sa-teeth-condition', 'no') !!}
							No
						</label>
					</div>

					{!! Form::submit('next step', array('class' => 'btn btn-primary')) !!}

				{!! Form::close() !!}

				</div>
			</div>
		</div>
		
	</div>
</section>

@include('partials.footer')