@include('partials.header')

<section class="container" id="main">
	<div id="survey_container">
		
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">

				@include('partials.form-head')

				<p class="text-center form-progress">Progress</p>
				<div class="progress">
					<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 37.5%;">
						37.5%
					</div>
				</div>

				<div class="step-title">
					<h3>Do you have sore or bleeding gums?</h3>
				</div>

				{!! Form::open(array('url' => '/sa-step3' )) !!}
					
					<div class="radio">
						<label>
							{!! Form::radio('sa-gum-condition', 'yes') !!}
							Yes
						</label>
					</div>

					<div class="radio">
						<label>
							{!! Form::radio('sa-gum-condition', 'no') !!}
							No
						</label>
					</div>

					{!! Form::submit('next step', array('class' => 'btn btn-primary')) !!}

				{!! Form::close() !!}

				</div>
			</div>
		</div>
		
	</div>
</section>

@include('partials.footer')