@include('partials.header')

<section class="container" id="main">
	<div id="survey_container">
		
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">

				@if(session('completed'))
					<div class="text-center completed">
						<h1 class="text-center"><i class="fa fa-check-square-o"></i></h1>
						<div class="progress">
							<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
								100%
							</div>
						</div>
						<h3>{{ session('completed') }}</h3>
					</div>
				@else

					@include('partials.form-head')

					@if(session('emailExistsMessage'))
						<div class="alert alert-danger">
							<p>{{ session('emailExistsMessage') }}</p>
						</div>
					@endif

					<p class="text-center form-progress">Progress</p>
					<div class="progress">
						<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 87.5%;">
							87.5%
						</div>
					</div>

					<div class="step-title">
						<h3>Enter your personal info</h3>	
					</div>

					{!! Form::open(array('url' => '/sa-step7' )) !!}
						
						<div class="radio">
							<label>Firstname</label>
							{!! Form::text('sa-firstname', null, ['class' => 'form-control']) !!}
						</div>

						<div class="radio">
							<label>Lastname</label>
							{!! Form::text('sa-lastname', null, ['class' => 'form-control']) !!}
						</div>

						<div class="radio">
							<label>Email</label>
							{!! Form::email('sa-email', null, ['class' => 'form-control']) !!}
						</div>

						<div class="radio">
							<label>Phone</label>
							{!! Form::number('sa-phone', null, ['class' => 'form-control']) !!}
						</div>

						{!! Form::submit('next step', array('class' => 'btn btn-primary')) !!}

					{!! Form::close() !!}

				@endif

				</div>
			</div>
		</div>
		
	</div>
</section>

@include('partials.footer')