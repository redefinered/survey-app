@include('partials.header')

<section class="container" id="main">
	<div id="survey_container">
		
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">

				@if (count($errors) > 0)
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif

				<div class="page-header">
					<h1>Recommend us.</h1>
					<p>Would you recommend Howard Family Dental and our staff to yourfamily and friends?</p>
				</div>

				<p class="text-center form-progress">Progress</p>
				<div class="progress">
					<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;">
						50%
					</div>
				</div>

				{!! Form::open(array('url' => '/step2' )) !!}

					<div class="radio">
						<label>
							{!! Form::radio('rpfs-recommendation', 'yes') !!}	
							Yes
						</label>
					</div>

					<div class="radio">
						<label>
							{!! Form::radio('rpfs-recommendation', 'no') !!}
							no
						</label>
					</div>

					{!! Form::submit('next step', array('class' => 'btn btn-primary')) !!}	

				{!! Form::close() !!}

				</div>
			</div>
		</div>
		
	</div>
</section>

@include('partials.footer')