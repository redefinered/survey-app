@include('partials.header')

<section class="container" id="main">
	<div id="survey_container">
		
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">

					@if(session('completed'))
						<div class="text-center completed">
							<h1 class="text-center"><i class="fa fa-check-square-o"></i></h1>
							<h3>{{ session('completed') }}</h3>
						</div>
						<?php
							$progress = 100;
						?>
					@else
						<div class="page-header">
							<h1>Thanks for your interest!</h1>
							<p>Please enter your personal information.</p>
						</div>
						<?php
							$progress = 25;
						?>
					@endif

					@if(session('emailExistsMessage'))
						<div class="alert alert-danger">
							<p>{{ session('emailExistsMessage') }}</p>
						</div>
					@endif

					@if (count($errors) > 0)
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif

					<p class="text-center form-progress">Progress</p>
					<div class="progress">
						<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="<?php echo $progress ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $progress ?>%;">
							<?php echo $progress . '%'; ?>
						</div>
					</div>

					{!! Form::open(array('url' => '/reputation-system' )) !!}

						{!! Form::hidden('rpfs-status', 'incomplete') !!}
						<div class="form-group">
							<label>Firstname</label>
							{!! Form::text('rpfs-firstname', null, array('class' => 'form-control' )) !!}	
						</div>

						<div class="form-group">
							<label>Lastname</label>
							{!! Form::text('rpfs-lastname', null, array('class' => 'form-control')) !!}	
						</div>

						<div class="form-group">
							<label>Email</label>
							{!! Form::email('rpfs-email', null, array('class' => 'form-control')) !!}	
						</div>

						{!! Form::submit('next step', array('class' => 'btn btn-primary')) !!}

					{!! Form::close() !!}

				</div>
			</div>
		</div>
		
	</div>
</section>

@include('partials.footer')