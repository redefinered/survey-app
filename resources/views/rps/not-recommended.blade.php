@include('partials.header')

<section class="container" id="main">
	<div id="survey_container">
		
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">

				@if (count($errors) > 0)
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif

				<div class="page-header">
					<p>We are sorry your experience with Howard Family Dental and our staff did not meet your expectaions. Please leave us feedback so that we can work harder to meet your expectations in the future. We read each comment left and will do our best to win back your confidence</p>	
				</div>

				<p class="text-center form-progress">Progress</p>
				<div class="progress">
					<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%;">
						75%
					</div>
				</div>

				{!! Form::open(array('url' => '/not-recommended')) !!}
					<div class="form-group">
						{!! Form::textarea('rpfs-comment', null, ['class' => 'form-control']) !!}
					</div>
					{!! Form::submit('submit', array('class' => 'btn btn-default')) !!}
				{!! Form::close() !!}

				</div>
			</div>
		</div>
		
	</div>
</section>

@include('partials.footer')