@include('partials.header')

<section class="container social-form" id="main">
	<div id="survey_container">
		
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">

				@if (count($errors) > 0)
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif

				<div class="page-header">
					<h1>Shoot Us a Review!</h1>
					<p>Please choose your preferred review site:</p>
				</div>

				<p class="text-center form-progress">Progress</p>
				<div class="progress">
					<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%;">
						75%
					</div>
				</div>

				{!! Form::open(array('url' => '/social-review-options')) !!}
					
					<div class="social-page-options">
						<ul class="list-unstyled">
							<li class="facebook">
								<a href="https://www.facebook.com/howardfamdental" target="_blank" data-option="fb">
									<span class="icon"><i class="fa fa-facebook"></i></span>
									<span class="name">Facebook</span>
								</a>
							</li>
							<li class="google-plus">
								<a href="https://www.google.com/maps/place/Howard+Family+Dental/@31.8522835,-81.6100172,17z/data=!4m2!3m1!1s0x88fae47fca36569d:0x1ca76548e7faf7c3" target="_blank" data-option="gp">
									<span class="icon"><i class="fa fa-google-plus"></i></span>
									<span class="name">Google Plus</span>
								</a>
							</li>
							<li class="yelp">
								<a href="https://www.yelp.com/writeareview/biz/cYDfWrysunjekAv8WgrC4g?return_url=%2Fbiz%2FcYDfWrysunjekAv8WgrC4g" target="_blank" data-option="yp">
									<span class="icon"><i class="fa fa-yelp"></i></span>
									<span class="name">Yelp</span>
								</a>
							</li>
						</ul>
					</div>

					{!! Form::hidden('rpfs-social-option', 'empty'); !!}
					{!! Form::submit('finish', array('class' => 'btn btn-primary')) !!}

				{!! Form::close() !!}

				</div>
			</div>
		</div>
		
	</div>
</section>

@include('partials.footer')