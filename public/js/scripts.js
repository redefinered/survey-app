jQuery(function($){

	socialButton = $('.social-page-options a');

	socialButton.on('click', function(e){
		optionValue = $(this).data('option');
		$('input[name="rpfs-social-option"]').val(optionValue);
		$('input[type="submit"]').show();
	});

});